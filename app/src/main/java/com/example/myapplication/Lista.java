package com.example.myapplication;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplication.adapter.CoctelesAdapter;
import com.example.myapplication.modelos.Coctel;
import com.example.myapplication.modelos.CoctelesService;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class Lista extends Fragment {

    RecyclerView rc;


    public Lista() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lista, container, false);
        rc = view.findViewById(R.id.rc);
        GridLayoutManager gm = new GridLayoutManager(getContext(),3,GridLayoutManager.VERTICAL,false);
        rc.setLayoutManager(gm);
        CoctelesAdapter adapter = new CoctelesAdapter(CoctelesService.cocteles,R.layout.cotel_row, (Activity) getContext());
        rc.setAdapter(adapter);
        carGarDatos();
        return view;
    }

    public void carGarDatos(){
        DatabaseReference base = FirebaseDatabase.getInstance().getReference();
        DatabaseReference cocteldb = base.child("Cocteles");
        cocteldb.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Coctel coctel = snapshot.getValue(Coctel.class);
                coctel.setUuid(snapshot.getKey());
                if(!CoctelesService.cocteles.contains(coctel)){
                    CoctelesService.addCoctel(coctel);
                }

                rc.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                Coctel coctel = snapshot.getValue(Coctel.class);
                coctel.setUuid(snapshot.getKey());
                if(CoctelesService.cocteles.contains(coctel)){
                    CoctelesService.updateCoctel(coctel);
                }

                rc.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {
                Coctel coctel = snapshot.getValue(Coctel.class);
                coctel.setUuid(snapshot.getKey());
                if(CoctelesService.cocteles.contains(coctel)){
                    CoctelesService.removeCoctel(coctel);
                }

                rc.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}