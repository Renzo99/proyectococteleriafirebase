package com.example.myapplication.adapter;


import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.myapplication.R;
import com.example.myapplication.modelos.Coctel;
import com.example.myapplication.modelos.CoctelDetalle;

import java.util.List;


public class CoctelesAdapter extends RecyclerView.Adapter<CoctelesAdapter.CoctelesAdaptarHolder> {
    private List<Coctel> cocteles;
    int layout;
    private Activity activity;
    private View view;

    public CoctelesAdapter(List<Coctel> cocteles, int layout, Activity activity) {
        this.cocteles = cocteles;
        this.layout = layout;
        this.activity = activity;
    }

    @NonNull
    @Override
    public CoctelesAdaptarHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(layout,parent,false);

        return new CoctelesAdaptarHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CoctelesAdaptarHolder holder, int position) {
        Coctel coctel = cocteles.get(position);
        holder.nombre.setText(coctel.getNombre());
        Glide.with(activity).load(coctel.getUrl()).into(holder.img);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Coctel c = cocteles.get(position);
                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.container,new CoctelDetalle(c)).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return cocteles.size();
    }

    public class CoctelesAdaptarHolder extends RecyclerView.ViewHolder{
        TextView nombre;
        ImageView img;
        public CoctelesAdaptarHolder(@NonNull View itemView) {
            super(itemView);
            nombre = itemView.findViewById(R.id.nombre);
            img = itemView.findViewById(R.id.foto);
        }
    }
}
