package com.example.myapplication.modelos;

import java.util.ArrayList;

public class CoctelesService {
    public static ArrayList<Coctel> cocteles = new ArrayList<>();

    public static void addCoctel(Coctel coctel){
        cocteles.add(coctel);
    }

    public static void removeCoctel(Coctel coctel){
        cocteles.remove(coctel);
    }

    public  static void updateCoctel(Coctel coctel){
        cocteles.set(cocteles.indexOf(coctel),coctel);
    }


}
