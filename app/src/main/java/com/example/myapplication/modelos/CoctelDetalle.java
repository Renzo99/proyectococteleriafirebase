package com.example.myapplication.modelos;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.myapplication.R;
import com.example.myapplication.databinding.FragmentCoctelDetalleBinding;

import java.util.Objects;

public class CoctelDetalle extends Fragment {
    private View view;
    private Coctel coctel;
    private FragmentCoctelDetalleBinding detalle;


    public CoctelDetalle() {
        // Required empty public constructor
    }
    public CoctelDetalle(Coctel coctel) {
        this.coctel =coctel;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        detalle = FragmentCoctelDetalleBinding.inflate(inflater);
        view = detalle.getRoot();

        cargarDatos();

        return view;

    }

    private void cargarDatos() {
        detalle.nombreDetalle.setText(coctel.getNombre());
        detalle.ingredientesDetalle.setText(coctel.getIngredientes());
        detalle.instruccionesDetalle.setText(coctel.getInstrucciones());
        Glide.with(requireContext()).load(coctel.getUrl()).into(detalle.fotoDetalle);
    }
}