package com.example.myapplication.modelos;

import java.util.Objects;

public class Coctel {
    private String uuid;
    private String nombre;
    private String ingredientes;
    private String instrucciones;
    private String url;

    public Coctel() {
    }

    public Coctel(String uuid, String nombre, String ingredientes, String instrucciones, String url) {
        this.uuid = uuid;
        this.nombre = nombre;
        this.ingredientes = ingredientes;
        this.instrucciones = instrucciones;
        this.url = url;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(String ingredientes) {
        this.ingredientes = ingredientes;
    }

    public String getInstrucciones() {
        return instrucciones;
    }

    public void setInstrucciones(String instrucciones) {
        this.instrucciones = instrucciones;
    }


    @Override
    public boolean equals(Object o) {
        return uuid.equals(((Coctel)o).uuid);
    }



    @Override
    public String toString() {
        return "Coctel{" +
                "uuid='" + uuid + '\'' +
                ", nombre='" + nombre + '\'' +
                ", ingredientes='" + ingredientes + '\'' +
                ", instrucciones='" + instrucciones + '\'' +
                '}';
    }
}
