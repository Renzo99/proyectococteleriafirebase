package com.example.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.myapplication.databinding.FragmentAddBinding;
import com.example.myapplication.modelos.Coctel;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;


public class Add extends Fragment {

    private FragmentAddBinding layoutadd;

    private static final int code_camera = 21;
    private static final int code_gallery = 22;

    private Uri uri_img;

    public Add() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        layoutadd = FragmentAddBinding.bind(inflater.inflate(R.layout.fragment_add, container, false));
        View view =layoutadd.getRoot();


        layoutadd.addFotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                File foto = new File(getContext().getExternalFilesDir(null),"test.jpg");
                uri_img = FileProvider.getUriForFile(getContext(),getContext().getPackageName()+".provider",foto);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,uri_img);
                startActivityForResult(intent,code_camera);
            }
        });

        layoutadd.addGaleriButtom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent,code_gallery);
            }
        });

        layoutadd.addCoctelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validar()){
                    enviarDato();
                }else {
                    Toast.makeText(getContext(),"Error",Toast.LENGTH_LONG).show();
                }
            }
        });


        // Inflate the layout for this fragment
        return view;
    }

    private void borrarTodo() {
        layoutadd.fotoCoctel.setImageBitmap(null);
        layoutadd.instruccionesInput.setText("");
        layoutadd.nombreInput.setText("");
        layoutadd.ingredientesInput.setText("");
        layoutadd.ingredientesInput.setText("");
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case code_gallery:
                if(data!=null){
                    uri_img = data.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(),uri_img);
                        bitmap = rotateImage(bitmap,90);
                        layoutadd.fotoCoctel.setImageBitmap(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case  code_camera:
                Bitmap bitmap = BitmapFactory.decodeFile(getContext().getExternalFilesDir(null)+"/test.jpg");
                bitmap = rotateImage(bitmap,90);
                layoutadd.fotoCoctel.setImageBitmap(bitmap);
                break;

        }
    }
    public static Bitmap rotateImage(Bitmap src, float degree)
    {
        // create new matrix
        Matrix matrix = new Matrix();
        // setup rotation degree
        matrix.postRotate(degree);
        Bitmap bmp = Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
        return bmp;
    }

    private boolean validar() {
        return Objects.requireNonNull(layoutadd.nombreInput.getText()).length() != 0
                && Objects.requireNonNull(layoutadd.ingredientesInput.getText()).length() != 0
                && Objects.requireNonNull(layoutadd.instruccionesInput.getText()).length() != 0;
    }

    private void enviarDato(){
        DatabaseReference base = FirebaseDatabase.getInstance().getReference();
        DatabaseReference cocteldb = base.child("Cocteles");
        DatabaseReference coctelobjeto = cocteldb.push();
        StorageReference storageReference = FirebaseStorage.getInstance().getReference();
        StorageReference folderRef = storageReference.child("Cocteles_Img");
        StorageReference fotoRef = folderRef.child((new Date().toString()));


        fotoRef.putFile(uri_img).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Task<Uri> uriTask = taskSnapshot.getStorage().getDownloadUrl();
                while (!uriTask.isSuccessful());
                Uri downloadurl = uriTask.getResult();

                Coctel coctel = new Coctel();
                coctel.setUuid(UUID.randomUUID().toString());
                coctel.setNombre(layoutadd.nombreInput.getText().toString());
                coctel.setIngredientes(layoutadd.ingredientesInput.getText().toString());
                coctel.setInstrucciones(layoutadd.instruccionesInput.getText().toString());
                coctel.setUrl(downloadurl.toString());
                coctelobjeto.setValue(coctel);
                borrarTodo();
            }
        });


    }
}